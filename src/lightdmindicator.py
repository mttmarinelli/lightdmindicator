# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__="Matteo Marinelli"

import subprocess
import gtk
import appindicator
import os.path

def disableHW(x):
    subprocess.call(["gksudo", "sh -c 'cp /etc/X11/xorg.conf.backup /etc/X11/xorg.conf; service lightdm restart;'"])

def enableHW(x):
    subprocess.call(["gksudo", "sh -c 'rm /etc/X11/xorg.conf; service lightdm restart;'"])

def restartLDM(x):
    subprocess.call(["gksudo", "service lightdm restart"])

itemConnection = {
    0 : disableHW,
    1 : enableHW,
    2 : restartLDM
}

menuItem = {
    0 : "Disable Acceleration",
    1 : "Enable Acceleration",
    2 : "Restart LightDM"
}

if __name__ == "__main__":
    # icon path
    iconPath = "/usr/share/icons/"
    iconName = ["X11_enabled.png", "X11_disabled.png"]
    icon = ""
    
    # check xorg file
    if (os.path.isfile("/etc/X11/xorg.conf")):
        icon = iconPath + iconName[1] # HW disabled
    else:
        icon = iconPath + iconName[0] # HW enabled
    
    # id - icon name - category
    indicator = appindicator.Indicator (
        "lightdmIndcator", 
        icon,
        appindicator.CATEGORY_APPLICATION_STATUS)
    indicator.set_status (appindicator.STATUS_ACTIVE)
    
    # create a menu
    menu = gtk.Menu();
    
    # create some items
    for i in range(3):
        menu_items = gtk.MenuItem(menuItem[i])
        menu.append(menu_items)
        
        # Connect menu item up with a function
        menu_items.connect("activate", itemConnection[i])
        
        # show the items
        menu_items.show()
    
    indicator.set_menu(menu)
    gtk.main()
    
 
